import cgi, sys
from os import curdir, sep
from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer

from DataDispatcher import *

class MyHandler(BaseHTTPRequestHandler):
	'''
		HTTP request handler
	'''

	def do_POST(self):
		'''
			All requests are POST queries from the services
		'''
		ctype, pdict = cgi.parse_header(self.headers.getheader('content-type'))
		
		if ctype == 'application/x-www-form-urlencoded':
			length = int(self.headers.getheader('content-length'))
			postvars = cgi.parse_qs(self.rfile.read(length), keep_blank_values=1)
			self.send_response(200)
			self.end_headers()
			global data_dispatcher
			
			#new file request
			if postvars['req_type'][0] == 'FILE':
				filename = data_dispatcher.get_current_file(postvars['source'][0])
				f = open(data_dispatcher.get_current_file(postvars['source'][0]), 'rb')
				data_dispatcher.increase_file_no(postvars['source'][0])
				self.wfile.write(f.read())
				f.close()
	
			#new file information request
			if postvars['req_type'][0] == 'SIZE':
				curr_file = data_dispatcher.get_current_file(postvars['source'][0])
				file_size = data_dispatcher.get_file_size(curr_file)
                                if file_size > 0:
					self.wfile.write(str(file_size) + "*" + curr_file)
                                else:
                                        self.wfile.write(str(file_size))

			#push statistics from the service to the DataDispatcher
			if postvars['req_type'][0] == 'STATS':
				data_dispatcher.push_stats(postvars['source'][0], postvars['exec_time'][0], postvars['transfer_time'][0], postvars['data_size'][0])
	
				if data_dispatcher.sampling_is_done():
					if data_dispatcher.dynamic_partitioning:
						data_dispatcher.prepare_next_data(postvars['source'][0])
					else:
						if not data_dispatcher.test_over() and not data_dispatcher.data_split:
							print "test is not over"
							data_dispatcher.static_data_split()
def main():
	if len(sys.argv) < 3:
		print "Usage: python server.py data_directory scheduling_type"
		sys.exit(1)
	
	#the DataDispatcher stores global information about the data partioning
	global data_dispatcher
	
	if sys.argv[2] == "dynamic":
		data_dispatcher = DataDispatcher(sys.argv[1], True, "config_file")
	else:
		data_dispatcher = DataDispatcher(sys.argv[1], False, "config_file")

	try:
		server = HTTPServer(('', 12345), MyHandler)
		print "started httpserver..."
		server.serve_forever()
	
	except KeyboardInterrupt:
		print "stopping server..."
		server.socket.close()

if __name__ == '__main__':
	main()
