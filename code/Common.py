def file_size(filename):
	file = open(filename, 'rb')
        file.seek(0, 2)
        size = file.tell()
        file.close()

        return size

def list_of_files_size(filenames):
	total_size = 0
	for filename in filenames:
		total_size = total_size + file_size(filename)
	
	return total_size	
