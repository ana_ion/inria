from os import listdir
from os.path import isfile, join
import fileinput
import time
from Common import *

class DataDispatcher:
	'''
	DataDispatcher stores global information regarding data partitioning
	between BS and BD	
	'''
	
	def __init__(self, path, dynamic, config_file):
		'''
		Class constructor
		
		@type	path: str
		@param 	path: location of the input data
		
		@type	dynamic: boolean
		@param	dynamic: True if dynamic data partitioning will be used
		'''

		self.config_file = config_file
		self.parse_input_file()
		self.path = path
		self.files = [ path + "/" + f for f in listdir(path) if isfile(join(path, f)) ]
		self.site_files = {'BS' : [], 'BD' : []}
		self.index = {'BS' : 0, 'BD' : 0}
		self.sampling_file = path + '/sample_data'	
		self.sampling_done = {'BS' : False, 'BD' : False}
		self.stats = {'BS' : [], 'BD' : []}
		self.data_split = False
		
		self.dynamic_partitioning = dynamic
		self.data_size = {'BS' : 0, 'BD' : 0}
		self.global_index = 0
		self.current_size = 0

		self.total_size = list_of_files_size(self.files)
		self.generate_sampling_data()

		self.total_cost = self.no_units * self.unit_cost
		self.cloud_running_time = 0
		self.cloud_time_to_complete = 0
		self.available_time = self.cloud_execution_unit
		self.optimize_cost = True		

	def parse_input_file(self):
		'''
		Read execution parameters from the input file
		'''

		f = open(self.config_file, 'r')
		for line in f:
			key, value = line.split(":")
			key = key.strip(' \n')
			value = value.strip(' \n')
			setattr(self, key, float(value))
		f.close()		

	def increase_file_no(self, site):
		'''
		Increase the index of the next file in site's file list
		
		@type	site: str
		@param	site: name of the site(BS/BD)
		'''

		if not self.sampling_done[site]:
			self.sampling_done[site] = True
		else:
			self.index[site] = self.index[site] + 1
	
	def get_current_file(self, site):
		'''
		Get the following file in the site's file list

		@type   site: str
                @param  site: name of the site(BS/BD)

		@return	: name of the file
		'''

		if not self.sampling_done[site]:
			return self.sampling_file

		if not self.dynamic_partitioning and not self.data_split:
			return None
		
		if self.index[site] == len(self.site_files[site]):
			return None
		
		return self.site_files[site][self.index[site]]

	def get_file_size(self, filename):
		'''
		Get the size of the file filename

		@return	: -1 (test is over)
			   0 (no filename is provided - None)
			  size in B
		'''
		
		if self.test_over():
			return -1
		if filename is None:
			return 0
		
		return file_size(filename)

	def generate_sampling_data(self):
		'''
		Generate data to be used for sampling, as a fraction of the total
		input data
		'''

		sampling_size = self.sampling_percentage * 0.05
		current_sampling_size = 0
		sample_files = []

		for file in self.files:
			if current_sampling_size >= sampling_size:
				break;
			sample_files.append(file)
			current_sampling_size  = current_sampling_size + self.get_file_size(file)
	
		with open(self.sampling_file, 'w') as fout:
			for line in fileinput.input(sample_files):
				fout.write(line)
	
	def push_stats(self, site, exec_time, transfer_time, data_size):
		'''
		Add new statistic to the log and increase the cloud's cost consumption
		
		@type	site: str
		@param	site: name of the site sending the stats (BS/BD)

		@type	time: str
		@param	time: time spend running $data_size data

		@type	data_size: str	
		@param	data_size: input data size
		'''
		print "cloud_time_to_run: " + str(self.cloud_time_to_complete)

		comp_speed = float(data_size) / float(exec_time)
		transfer_speed = float(data_size) / float(transfer_time)
		self.stats[site].append([float(exec_time), float(transfer_time), float(data_size), comp_speed, transfer_speed])
	
		if site == 'BS':
			current_time = time.time()
			self.cloud_running_time += float(exec_time)
			self.cloud_running_time += float(transfer_time)
			self.available_time = self.available_time - float(exec_time) - float(transfer_time)
		
		while len(self.stats[site]) > self.log_size:
			del self.stats[site][0]	
		
		print "length: " + str(len(self.stats[site]))		

	def get_cloud_time_to_run(self, files):
		'''
		Approximate the time necessary for running all the MapReduce operations over
		the $files input data if only the cloud environment is used

		@type	files: [str]
		@param	files: list of input files 
		'''
		
		size = list_of_files_size(files)
		time_to_run = size * self.stats['BS'][0][0] / self.stats['BS'][0][2]
		time_to_run += size * self.stats['BS'][0][1] / self.stats['BS'][0][2] 

		return time_to_run

	def deadline_penalty(self, files):
		'''
		Determine if executing the files listed in $files on DesktopGrid
		will determine the execution to last longer than executing all
		MapReduce operations in the cloud
		
		@return	: True - execution will take longer
		'''
		
		average_comp_speed = 0
		average_transfer_speed = 0
                for stat_info in self.stats['BD']:
			average_comp_speed += stat_info[3]
			average_transfer_speed += stat_info[4]
                average_comp_speed /= len(self.stats['BD'])
		average_transfer_speed /= len(self.stats['BD'])

		bd_estimated_comp_time = list_of_files_size(files) / average_comp_speed
		bd_estimated_transfer_time  = list_of_files_size(files) / average_transfer_speed

		if bd_estimated_comp_time + bd_estimated_transfer_time + self.cloud_running_time < self.cloud_time_to_complete:
			return False
		
		return True

	def sampling_is_done(self):
		'''
		Test if the sampling phase is over for both sites

		@return	: True - sampling is done
			  False - sampling is not over for at least one site
		'''

		if len(self.stats['BS']) == 1:
			self.cloud_time_to_complete = self.get_cloud_time_to_run(self.files)		
	
		if len(self.stats['BS']) >= 1 and len(self.stats['BD']) >= 1:
			return True
		
		return False 		

	def static_data_split(self):
		'''
		Partition data between the two sites using the information obtained during the
		sampling phase
		'''

		bs_time = self.stats['BS'][0][0] + self.stats['BS'][0][1]
		bd_time = self.stats['BD'][0][0] + self.stats['BD'][0][1]
		ratio = bd_time / (bd_time + bs_time)
		bs_size = self.total_size * ratio
		bd_size = self.total_size * (1 - ratio)
		current_bs_size = 0
		current_bd_size = 0

		for file in self.files:
			if current_bs_size >= bs_size:
				self.site_files['BD'].append(file)
				current_bd_size = current_bd_size + self.get_file_size(file)
			else:
				self.site_files['BS'].append(file)
				current_bs_size = current_bs_size + self.get_file_size(file)
		
		self.data_split = True
		self.global_index = len(self.files)				
	
	def need_to_rent_more_resources(self):	
		'''
		Check if there are enough cloud resources for another MapReduce phase

		@return	: True - need to extend the lease period 
		'''

		standard_time = self.stats['BS'][0][0]
	
		if self.available_time <= standard_time:
			return True
		
		return False		

	def final_round(self):
		'''
		Determine whether the data left to be processed will necessitate extending the
		cloud resources period lease
		
		@return	: True - the time left to process the remaining data (if only the cloud
				would be used) is less than an entire lease period
		'''
	
		time_left = self.get_cloud_time_to_run(self.files[self.global_index:len(self.files)])
		if time_left < self.cloud_execution_unit:
			return True
		
		return False		
		
	def volatility_factor(self, average):
		'''
		Compute the DesktopGrid's volatility factor as the average standard deviation of the 
		site's computational speed

		@type	average: float
		@param	average: average computation speed

		@return	: float - volatility factor
		'''
		
		volatility_factor = 0
		for stat_info in self.stats['BD']:
			deviation = max(stat_info[2], average) - min(stat_info[2], average)
			volatility_factor = volatility_factor + deviation

		return volatility_factor / len(self.stats['BD'])
 
	def add_new_data(self, site):
		'''
		Generate the next list of chunks for site $site

		@type	site: str
		@param	site: name of the site(BS/BD)
		'''

		current_size = 0
		self.site_files[site] = []
		self.index[site] = 0
	
		#first run after the sampling phase
		#use the result from the sampling phase
		if len(self.stats['BS']) == 1 and len(self.stats['BD']) == 1:
			bs_time = self.stats['BS'][0][0] + self.stats['BS'][0][1]
			bd_time = self.stats['BD'][0][0] +  self.stats['BD'][0][1]
			ratio = bd_time / (bs_time + bd_time)
			self.data_size['BS'] = self.global_standard_size * ratio
			self.data_size['BD'] = self.global_standard_size - self.data_size['BS']
		
		#compute average speed using the logged data and decide the size of the next partition
		else:
			if site == 'BD':
				average = 0
				for stat_info in self.stats[site]:
					average = average + stat_info[3]
				average = average / len(self.stats[site])
				av_st_dev = self.volatility_factor(average)
				
				latest_speed = self.stats[site][len(self.stats[site]) - 1][3]
				if latest_speed > (average + av_st_dev):
					self.data_size[site] = self.data_size[site] * latest_speed / (average + av_st_dev)
				if latest_speed < (average - av_st_dev):
					self.data_size[site] = self.data_size[site] * latest_speed / (average - av_st_dev)
	
			if site == 'BS':
				if self.need_to_rent_more_resources():
					if self.optimize_cost:
						if self.final_round() and not self.deadline_penalty(self.files[self.global_index:len(self.files)]):
							self.data_size[site] = 0
							self.site_files[site] = []
						else:
							self.available_time += self.cloud_execution_unit
							self.total_cost = self.total_cost + self.unit_cost * self.no_units			
					else:
						self.available_time += self.cloud_execution_unit
                                                self.total_cost = self.total_cost + self.unit_cost * self.no_units


		print site + " " + str(self.data_size[site])
		print "time available: " +  str(self.available_time)

		#self.data_size['BD'] = 0

		while current_size < self.data_size[site]:
			self.site_files[site].append(self.files[self.global_index])
			current_size = current_size + file_size(self.files[self.global_index])
			self.global_index  = self.global_index + 1
			if self.global_index == len(self.files):
				break

	def prepare_next_data(self, site):
		'''
		Method called by the HTTP server when a site completes execution (when
		the site pushed some new statistics)

		@type   site: str
                @param  site: name of the site(BS/BD)
		'''

		if self.global_index < len(self.files):
			self.add_new_data(site)		
			if len(self.site_files['BS']) == 0:
				self.add_new_data('BS')
			if len(self.site_files['BD']) == 0:
				self.add_new_data('BD')

	def test_over(self):
		'''
		Test if all the input data has been partitioned
		
		@return	: True - no more data left
		'''

		if self.global_index == len(self.files) and self.index['BS'] == len(self.site_files['BS']) and self.index['BD'] == len(self.site_files['BD']):
			return True
		
		return False
	
