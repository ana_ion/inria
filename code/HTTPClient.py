import urllib2, urllib

class HTTPClient(object):
	def __init__(self, url_server, client_name):
                '''
        	Class constructor - this class contains the methods used for
        	interacting with the HTTP server
                
        	@type   url_server: str
        	@param  url_server: hostname of the HTTP server where the input data
        	is stored
        
        	@type   client_name: str
        	@param  client_name: name of the HTTP client (BS/BD)
        	
        	'''
		self.url_server = url_server
                self.client_name = client_name
		self.total_transfer_time = 0
		self.total_execution_time = 0		
	
	def get_file_info(self):
        	'''
        	Request file information: size and name
        	
        	@return : [size, filename]
        	'''

       		req_data = {'req_type' : 'SIZE', 'source' : self.client_name}
        	data = urllib.urlencode(req_data)
        	req = urllib2.Request(self.url_server, data = data)
        	f = urllib2.urlopen(req)
        	response = f.read()
        	tokens = response.split("*")
		
        	return tokens

	def request_file(self, filename):
        	'''
        	Request actual file using the information previously
        	received.
        
		@type   filename: str
        	@param  filename: name of the file to be requested from the server
		
        	@return : 
        	'''

        	req_data = {'req_type' : 'FILE', 'source' : self.client_name}
        	data = urllib.urlencode(req_data)
        	req = urllib2.Request(self.url_server, data = data)
        	f = urllib2.urlopen(req)
        	output = open(filename, 'w')
        	output.write(f.read())
        	output.close()

	def push_stats(self, execution_time, transfer_time, data_size):
        	'''
        	Send to the http server information regarding the execution: time
        	spent running data_size

        	@type   execution_time: float
        	@param  execution_time: time spent executing $data_size data on $client_name
		
        	@type   data_size: int
        	@param  data_size: size of the data that was executed in $execution_time
                           	   on $client_name
        	'''

        	post_data = {'req_type' : 'STATS', 'source' : self.client_name, 'exec_time' : execution_time, 'transfer_time': transfer_time, 'data_size' : data_size}
        	data = urllib.urlencode(post_data)
        	req = urllib2.Request(self.url_server, data = data)
        	f = urllib2.urlopen(req)
