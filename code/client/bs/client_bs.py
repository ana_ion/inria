import urllib2, urllib, time, subprocess, fileinput, sys, random, os
sys.path.append('../../')
from Common import *
from HTTPClient import * 

class BSClient(HTTPClient):
	def __init__(self, url_server):
		'''
        	Class constructor. Inherits the HHTPClient class

        	@type   url_server: str
        	@param  url_server: name of the HTTP server storing the data
        	'''
		
		super(BSClient, self).__init__(url_server, 'BS')

	def clean_env(self, filename):
		'''
       		Remove from BSFS any files created during the execution

        	@type   filename: str
        	@param  filename: name of the input file used for the execution
        	'''
		
		command = "../../../../hadoop-1.0.4/bin/hadoop fs -rmr /output-wordcount"
        	clean = subprocess.Popen(command.split())
        	clean.communicate()

        	command = "../../../../hadoop-1.0.4/bin/hadoop fs -rm " + filename
        	clean = subprocess.Popen(command.split())
        	clean.communicate()

	def run_test(self, filename):
		'''
        	Run MapReduce over BlobSeer for the input stored in $filename

        	@type   filename: str
        	@param  filename: name of the file containing the data

        	@return : execution_time
        	'''

        	command = "../../../../hadoop-1.0.4/bin/hadoop fs -copyFromLocal " + filename + " " + filename
		subprocess.Popen(command.split(), stdout = subprocess.PIPE)

		command = "../../../../hadoop-1.0.4/bin/hadoop jar ../../../../hadoop-1.0.4/hadoop-examples-1.0.4.jar wordcount " + filename + " /output-wordcount"
        
		start_time = time.time()

		bs_job = subprocess.Popen(command.split(), stdout=subprocess.PIPE)
        	output = bs_job.communicate()[0]
	
		end_time = time.time()
	
		execution_time = end_time - start_time
		print str(execution_time)
		push_stats(execution_time, Common.file_size(filename))
	
		self.clean_env(filename)

		return execution_time
	
	def pack_test_files(self, test_files):
		'''
        	Concatenate the chunks received from the server in a single file
        	that is provided to Hadoop
        
        	@type   test_files: [str]
        	@param  test_files: list of files to be concatenated
        	'''

		index = random.randint(0, 1000000)
		with open('data/test_file_' + str(index), 'w') as fout:
			for line in fileinput.input(test_files):
				fout.write(line)

		return 'data/test_file_' + str(index)
 
	def schedule_dynamic(self):
		'''
        	Request data during runtime. The client has no knowledge about the
        	partitioning technique. It continuously polls the server until no
        	data is sent. 
        	'''
	
		new_chunk = True
		test_files = []
		total_no_files = 0	
		transfer_time = 0

		while True:
			info = self.get_file_info()
			if info[0] == '-1':
				total_no_files += len(test_files)
				self.total_transfer_time += transfer_time
				if len(test_files) > 0:
                                        execution_time = random.uniform(1.0, 2.0) * size / 1000000
                                        self.total_execution_time += execution_time
                                        time.sleep(2)
                                        self.push_stats(execution_time, transfer_time, size)
				break
			if info[0] == '0':
				if new_chunk:
					#test_filename = pack_test_files(test_files)
					#execution_time = run_test(test_filename)
					#execution_time = random.randint(1, 4)		
					size = list_of_files_size(test_files)
					execution_time = 0.2 * size / 1000000
					self.total_execution_time += execution_time
					self.total_transfer_time += transfer_time
					#time.sleep(execution_time)
					time.sleep(1)
					self.push_stats(execution_time, transfer_time, size)
					total_no_files += len(test_files)
					test_files = []
					transfer_time = 0
					new_chunk = False
				else:
					time.sleep(3)
				continue
			new_chunk = True
			start = time.time()
			self.request_file(info[1])
			transfer_time += (time.time() - start)
			print transfer_time
			test_files.append(info[1])
			
		print "Test is over. Received: " + str(total_no_files)
		print "Total execution time: " + str(self.total_execution_time)
		print "Total transfer time: " + str(self.total_transfer_time)

def main():
	client = BSClient('http://127.0.0.1:12345')
	client.schedule_dynamic()

if __name__ == '__main__':
	main()
