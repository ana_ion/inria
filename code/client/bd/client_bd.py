import urllib2, urllib, time, subprocess, sys, random
sys.path.append('../../')
from Common import *
from HTTPClient import *

class BDClient(HTTPClient):
	def __init__(self, url_server):
		'''
        	Class constructor. Inherits the HHTPClient class
	
        	@type   url_server: str
        	@param  url_server: name of the HTTP server storing the data        	
        	'''

                super(BDClient, self).__init__(url_server, 'BD')
	
	def clean_env(self):
		'''
        	Remove from the BD nodes any file generated during execution and stop
        	all processes
        	'''

		command = "taktuk -d-1 -l root -f all_hosts broadcast exec { killall java }"
        	clean = subprocess.Popen(command.split())
        	clean.communicate()

       		command = "taktuk -d-1 -l root -f all_hosts broadcast exec { rm -rf /tmp/mapred }"
        	clean = subprocess.Popen(command.split())
        	clean.communicate()

	def run_test(self, serv1, serv2):
		'''
        	Run the BS's MapReduce over the deployed nodes.
        
        	@type   serv1: str
        	@param  serv1: name of the server on which the master will run
        
        	@type   serv2: str
        	@param  serv2: name of the second server in BD deployment
        	'''

		command = "./run_wordcount.sh " + serv1 + " " + serv2
		subproc = subprocess.Popen(command.split())
	
		command = "/bin/bash try.sh " + serv1
		subproc = subprocess.Popen(command.split())
		subproc.communicate()	

	def schedule_dynamic(self):
        	'''
        	Request data during runtime. The client has no knowledge about the
        	partitioning technique. It continuously polls the server until no
        	data is sent. 
        	'''

		new_chunk = True
        	test_files = []
		total_no_files = 0	
		transfer_time = 0

        	while True:
                	info = self.get_file_info()
                	if info[0] == '-1':
				total_no_files += len(test_files)
				self.total_transfer_time += transfer_time
				if len(test_files) > 0:
					execution_time = random.uniform(1.0, 2.0) * size / 1000000
                                        self.total_execution_time += execution_time
					time.sleep(2)
					self.push_stats(execution_time, transfer_time, size)
				break
			if info[0] == '0':
                        	if new_chunk:
                                	#execution_time = run_test(test_filename) 	
					size = list_of_files_size(test_files)
					execution_time = random.uniform(0.5, 1.0) * size / 1000000
					self.total_execution_time += execution_time
					self.total_transfer_time += transfer_time
					#time.sleep(execution_time)
					time.sleep(2)
					self.push_stats(execution_time, transfer_time, size)
                                	total_no_files = total_no_files + len(test_files)
					test_files = []
					transfer_time = 0
                                	new_chunk = False
                        	else:
                                	time.sleep(3)
                        	continue
                	new_chunk = True
			start = time.time()
                	self.request_file(info[1])
			transfer_time += time.time() - start 
                	test_files.append(info[1])

		print "Test is over. Received: " + str(total_no_files)
		print "Total execution time: " + str(self.total_execution_time)
                print "Total transfer time: " + str(self.total_transfer_time)

def main():
	client = BDClient('http://127.0.0.1:12345')
        client.schedule_dynamic()

if __name__ == '__main__':
	main()
